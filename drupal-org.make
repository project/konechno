api = 2
core = 7.17

; Modules

projects[admin_menu][version] = 3.0-rc4
projects[ctools][version] = 1.3
projects[mollom][version] = 2.7
projects[shurly][version] = 1.1
projects[views][version] = 3.7

; Themes

projects[tao][version] = 3.0-beta4
projects[rubik][version] = 4.0-beta9
projects[cube][version] = 1.3
