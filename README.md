Конечно “Surely”
================

A simple [Drupal][] install profile for URL shortener sites using [ShURLy][]
by [Lullabot][]. Created by [Reveal IT][].

[Drupal]: http://drupal.org/
[ShURLy]: http://drupal.org/project/shurly
[Lullabot]: http://lullabot.com/
[Reveal IT]: http://revealit.dk/
