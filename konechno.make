; Drupal core
api = 2
core = 7.x
projects[] = drupal

; Contrib modules

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.3

projects[mollom][subdir] = contrib
projects[mollom][version] = 2.7

projects[shurly][subdir] = contrib
projects[shurly][version] = 1.1

projects[views][subdir] = contrib
projects[views][version] = 3.7

; Themes

projects[tao][subdir] = contrib
projects[tao][version] = 3.0-beta4

projects[rubik][subdir] = contrib
projects[rubik][version] = 4.0-beta9

projects[cube][subdir] = contrib
projects[cube][version] = 1.3

