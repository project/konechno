; Drupal core
api = 2
core = 7.x
projects[] = drupal

; Get Konechno
projects[konechno][type] = profile
projects[konechno][download][type] = git
projects[konechno][download][url] = http://git.drupal.org/project/konechno.git
projects[konechno][download][revision] = 7.x-1.3
